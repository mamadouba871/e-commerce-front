import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-social-user-password',
  templateUrl: './social-user-password.component.html',
  styleUrls: ['./social-user-password.component.scss']
})
export class SocialUserPasswordComponent implements OnInit {
  // @ts-ignore
  password1: string;
  // @ts-ignore
  password2: string;
  // @ts-ignore
  userRequest: UserRequest = {
    username: "",
    password: "",
    email: "",
    fname: "",
    lname: "",
    photoUrl: "",
    type: ""
  };
  // @ts-ignore
  message: string;

  private SERVER_URL = environment.SERVER_URL;
  constructor(private router: Router,
              private userService: UserService,
              private http: HttpClient,
              private spinner: NgxSpinnerService,
              private toast: ToastrService,
              private route: ActivatedRoute) {

  }

  ngOnInit(): void {
    this.userService.socialUserToRegister$.subscribe(data=>{
      if(this.route.snapshot.queryParams['isMessage']){
        this.message = "Veuillez créer votre mot de passe d'abord";
      }
      console.log(this.message);
      if(data){
        this.userRequest.email = data.socialUser.email;
        this.userRequest.username = data.socialUser.email.split('@')[0];
        this.userRequest.lname = data.socialUser.lastName;
        this.userRequest.fname = data.socialUser.firstName;
        this.userRequest.photoUrl = data.socialUser.photoUrl;
        this.userRequest.type = 'social';
      }
    })
  }

  register(f: NgForm) {
    const password = this.password1;
    if(f.invalid){
      return;
    }
    f.reset();
    this.userRequest.password = password;
    console.log(password);
    this.spinner.show().then(p=>{
      this.http.post(`${this.SERVER_URL}/auth/register`, this.userRequest)
        // @ts-ignore
        .subscribe((data: UserResponse)=>{
          if(data.success){
            this.toast.success(`${data.message}`, 'Création Compte', {
              timeOut: 1500,
              progressBar: true,
              progressAnimation: 'increasing',
              positionClass: 'toast-top-right'
            })
            this.spinner.hide();
            if(this.route.snapshot.queryParams['returnUrl']) {
              this.router.navigateByUrl(this.route.snapshot.queryParams['returnUrl']);
            }else {
              this.router.navigate(['/profile']);
            }
          }else {
            this.toast.error(`${data.message}`, 'Création Compte', {
              timeOut: 1500,
              progressBar: true,
              progressAnimation: 'increasing',
              positionClass: 'toast-top-right'
            })
            this.spinner.hide();
            this.router.navigate(['/profile']);
          }
        });
    });
  }
}
interface UserRequest {
  username: string;
  password: string;
  email: string;
  fname: string;
  lname: string;
  photoUrl: string;
  type: string;
}
interface UserResponse {
  message: string;
  success : boolean;
}

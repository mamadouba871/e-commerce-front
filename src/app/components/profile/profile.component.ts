import { Component, OnInit } from '@angular/core';
// @ts-ignore
import {AuthService, SocialUser} from 'angularx-social-login';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {OrderService} from '../../services/order.service';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  // @ts-ignore
  myUser: SocialUser|ResponseModel;
  // @ts-ignore
  myOrders: OrderResponde[];
  // @ts-ignore
  page: number = 1;
  // @ts-ignore
  end: boolean;

  constructor(private authService: AuthService,
              private userService: UserService,
              private orderService: OrderService,
              private router: Router) { }

  ngOnInit(): void {
    let data = JSON.parse(<string> localStorage.getItem('user'));
    if(data.length!==0){
      this.myUser = data;
      this.orderService.page$.subscribe(page=>{
        this.page = page;
        // @ts-ignore
        this.orderService.getOrdersOfClient(data.email, 5, this.page).subscribe((userOrdersWStatus:OrderResponseWStatus)=>{
          this.myOrders = userOrdersWStatus.orders;
          this.end = userOrdersWStatus.end;
        })
      })
    }


    // @ts-ignore
    /*this.userService.userData$
      .pipe(
        // @ts-ignore
        map((user: SocialUser | ResponseModel)=>{
          if(user instanceof SocialUser){
            return {
              ...user,
              email : "test@test.com"
            };
          }else {
            return user;
          }
        })
      )
      .subscribe((data: ResponseModel | SocialUser)=>{
        this.myUser = data;
      })*/
  }

  logout() {
    this.userService.logout();
  }

  nextPrevious(next: boolean){
    (next) ? this.page++ : this.page--;
    if(this.page<0) this.page = 1;
    this.orderService.page$.next(this.page);
  }

  selectOrder(id: number) {
    this.router.navigate(['/order', id]).then();
  }
}
interface ResponseModel {
  token: string;
  auth: boolean;
  email: string;
  username: string;
  firstName: string;
  lastName: string;
  photoUrl: string;
  userId: number;
  name: string;
}
interface OrderResponde {
  id: number;
  total: number;
}
interface OrderResponseWStatus {
  orders : OrderResponde[],
  end: boolean
}


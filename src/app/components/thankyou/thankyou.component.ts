import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {OrderService} from '../../services/order.service';

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.scss']
})
export class ThankyouComponent implements OnInit {
  // @ts-ignore
  message: string;
  // @ts-ignore
  orderId: number;
  // @ts-ignore
  products;
  // @ts-ignore
  cartTotal: number;
  // @ts-ignore
  thank: boolean;

  constructor(private router: Router,
              private orderService: OrderService) {
    const navigation = this.router.getCurrentNavigation();
    // @ts-ignore
    const state = navigation.extras.state as {
      message: string,
      products: ProductResponseModel[],
      orderId: number,
      total: number
    }
    console.log(state);
    if(state){
      this.thank = false;
      this.message = state.message;
      this.products = state.products;
      this.orderId = state.orderId;
      this.cartTotal = state.total;
    }else {
      this.thank = true;
    }
  }

  ngOnInit(): void {
  }

}
interface ProductResponseModel {
  id: number,
  name: string,
  description: string,
  price: number,
  quantity: number
  image: string
}

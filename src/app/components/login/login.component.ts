import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
// @ts-ignore
import {AuthService} from 'angularx-social-login';
import {ActivatedRoute, Route, Router} from '@angular/router';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  // @ts-ignore
  email: string;
  // @ts-ignore
  password: string;
  constructor(private authService: AuthService,
              private router: Router,
              private userService: UserService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.userService.authState$.subscribe(authState=>{
      if(authState){
        this.router.navigateByUrl(this.route.snapshot.queryParams['returnUrl']);
      }/*else {
        this.router.navigateByUrl('/login');
      }*/
    })
  }

  login(f: NgForm) {
    const email = this.email;
    const password = this.password;

    if(f.invalid){
      return;
    }
    f.reset();
    this.userService.loginUser(email, password);
  }

  signInWithGoogle() {
    this.userService.googleLogin();
  }
}

import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  private SERVER_URL = environment.SERVER_URL;

  // @ts-ignore
  products: Order[];
  // @ts-ignore
  message: string;
  // @ts-ignore
  success: boolean;

  constructor(private router: ActivatedRoute,
              private route: Router,
              private http: HttpClient,
              private cookieService: CookieService) { }

  ngOnInit(): void {
    this.router.paramMap.pipe(
      map((param: ParamMap)=>{
        return param.get('id');
      })
    ).subscribe(idOrder=>{
      // @ts-ignore
      this.http.get(`${this.SERVER_URL}/orders/`+idOrder, {
        params: {
          email : this.cookieService.get('email').toString()
        }
        // @ts-ignore
      }).subscribe((data: OrderResponse)=>{
        this.products = data.order;
        this.message = data.message;
        this.success = data.success;
      })
    })
  }

}
interface Order {
  name: string;
  image: string;
  total: number
}
interface OrderResponse {
  order: Order[];
  message: string;
  success: boolean;
}

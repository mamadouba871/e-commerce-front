import { Component, OnInit } from '@angular/core';
import {CartModelServer} from '../../models/cart.model';
import {CartService} from '../../services/cart.service';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  // @ts-ignore
  cartData: CartModelServer;
  // @ts-ignore
  cartTotal: number;
  // @ts-ignore
  authSate: boolean = false;
  // @ts-ignore
  textAcc: string;

  constructor(public cartService: CartService,
              private userService: UserService) { }

  ngOnInit(): void {
    this.cartService.cartTotal$.subscribe(total=>this.cartTotal = total);

    this.cartService.cartData$.subscribe(date=>this.cartData = date);

    this.userService.authState$.subscribe(authState=>this.authSate = authState);
    let data = JSON.parse(<string> localStorage.getItem('user'));
    if(data.length!==0){
      this.authSate = true;
    }
    /*if(this.authSate){
      this.textAcc = 'My Account';
    }else {
      this.textAcc = 'LOGIN';
    }*/
  }

}

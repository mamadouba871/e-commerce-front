import {AfterViewInit, Component, OnInit} from '@angular/core';
import {CartService} from '../../services/cart.service';
import {OrderService} from '../../services/order.service';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {CartModelServer} from '../../models/cart.model';
import {CookieService} from 'ngx-cookie-service';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit , AfterViewInit{

  // @ts-ignore
  cartTotal: number;
  // @ts-ignore
  cartData: CartModelServer;
  // @ts-ignore
  userEmail: string;

  constructor(private cartService: CartService, private orderService: OrderService,
  private router: Router, private spinner: NgxSpinnerService,
              private cookieService: CookieService, private userService: UserService) { }

  ngOnInit(): void {
    this.cartService.cartData$.subscribe(data=>this.cartData = data);
    this.cartService.cartTotal$.subscribe(total=>this.cartTotal = total);
    /*this.userService.userData$.subscribe(data=>this.userEmail = data.email)*/
    this.userEmail = this.cookieService.get('email');
  }

  ngAfterViewInit(): void { /*this.spinner.show();*/ }

  onCheckout() : void {
    this.spinner.show().then((p:any)=>{
      this.cartService.CheckoutFromCart(this.userEmail);
    });
  }
}

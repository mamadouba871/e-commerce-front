import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ProductService} from '../../services/product.service';
import {CartService} from '../../services/cart.service';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {tryCatch} from 'rxjs/internal-compatibility';
import {ProductModelServer} from '../../models/product.model';

declare let $: any;

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit, AfterViewInit {

  // @ts-ignore
  id: number;
  // @ts-ignore
  product: ProductModelServer;
  thumbImages: any[] = [];

  // @ts-ignore
  @ViewChild('quantity') quantityInput;

  constructor(private productService: ProductService,
              private cartService: CartService,
              private router: ActivatedRoute,
              private route: Router) { }

  ngOnInit(): void {
    this.router.paramMap.pipe(
      map((param: ParamMap)=>{
        return param.get('id');
      })
    ).subscribe(prodId=>{
      // @ts-ignore
      if (isNaN(prodId)) {
        this.route.navigate(['/']).then();
      } else {
        // @ts-ignore
        this.id = Number.parseInt(prodId);
        this.productService.getSingleProduct(this.id).subscribe(prod => {
          this.product = prod;
          console.log(this.product);
          console.log('prod');
          if (prod.images !== null) {
            this.thumbImages = prod.images.split(';');
          }
        });
      }
    });
  }

  ngAfterViewInit(): void {
    // Product Main img Slick
    $('#product-main-img').slick({
      infinite: true,
      speed: 300,
      dots: false,
      arrows: true,
      fade: true,
      asNavFor: '#product-imgs',
    });

    // Product imgs Slick
    $('#product-imgs').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: true,
      centerMode: true,
      focusOnSelect: true,
      centerPadding: 0,
      vertical: true,
      asNavFor: '#product-main-img',
      responsive: [{
        breakpoint: 991,
        settings: {
          vertical: false,
          arrows: false,
          dots: true,
        }
      },
      ]
    });

    // Product img zoom
    var zoomMainProduct = document.getElementById('product-main-img');
    if (zoomMainProduct) {
      $('#product-main-img .product-preview').zoom();
    }

  }

  addToCart(id: number) {
    this.cartService.AddProductToCart(id, this.quantityInput.nativeElement.value);
  }

  increase() {
    let value = parseInt(this.quantityInput.nativeElement.value);
    if(this.product.quantity >= 1){
      value++;
      if(value > this.product.quantity){
        value = this.product.quantity;
      }
    }else {
      return;
    }
    this.quantityInput.nativeElement.value = value.toString();
  }

  decrease() {

    let value = parseInt(this.quantityInput.nativeElement.value);
    if(this.product.quantity >= 0){
      value--;
      if(value <= 0){
        value = 1;
      }
    }else {
      return;
    }
    this.quantityInput.nativeElement.value = value.toString();
  }
}

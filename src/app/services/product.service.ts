import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable, Subject} from 'rxjs';
import {ProductModelServer, ServerResponse} from '../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private SERVER_URL = environment.SERVER_URL;
  //private title = 'WebSockets chat';
  // @ts-ignore
  private stompClient;
  productSubject = new Subject<any[]>();


  constructor(private  http: HttpClient) { }

  getAllProducts(numberOfResults = 12): Observable<ServerResponse>{
    return this.http.get<ServerResponse>(this.SERVER_URL + "/products", {
      params: {
        limit: numberOfResults.toString()
      }
    });
  }

  getSingleProduct(id: number): Observable<ProductModelServer>{
    return this.http.get<ProductModelServer>(this.SERVER_URL + '/products/' + id);
  }

  getProductsFromCategory(catName: string) : Observable<ProductModelServer[]>{
    return this.http.get<ProductModelServer[]>(this.SERVER_URL + '/products/category/' + catName);
  }

  /*initializeWebSocketConnection(){
    let ws = new SockJS()
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function(frame) {
      that.stompClient.subscribe("/socket-send", (message) => {
        if(message.body) {
          if(that.incidents==null){
            that.incidents = new Array();
          }
          that.incidents.push((message.body).replace(/\"([^(\")"]+)\":/g,"$1:"));
          that.emitIncidentSubject();
          console.log("json : ----"+that.incidents[0].split(",")[4].split(":")[1].split("}")[0]);
          console.log("proba service ---------"+(message.body).probabilite)
        }
      });
    });
  }*/


}

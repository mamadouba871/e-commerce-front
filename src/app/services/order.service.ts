import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private products : ProductResponseModel[] = [];
  private SERVER_URL = environment.SERVER_URL;

  page$ = new BehaviorSubject<number>(1);

  constructor(private http: HttpClient) { }

  getSingleOrder(orderId: number){
    return this.http.get<ProductResponseModel[]>(this.SERVER_URL + '/orders/' + orderId).toPromise();
  }

  getOrdersOfClient(email: string, limit: number, page: number = 1){
    return this.http.get<OrderResponseWStatus>(`${this.SERVER_URL}/orders/user/${email}`, {
      params: {
        limit: limit.toString(),
        page: page.toString()
      }
    })
  }
}
interface ProductResponseModel {
  id : number,
  title : string,
  description : string,
  price : number,
  image : string,
  quantityOrdered : number
}
interface OrderResponde {
  id: number;
  total: number;
}
interface OrderResponseWStatus {
  orders : OrderResponde[],
  end: boolean
}

import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ProductService} from './product.service';
import {OrderService} from './order.service';
import {environment} from '../../environments/environment';
import {CartModelPublic, CartModelServer} from '../models/cart.model';
import {BehaviorSubject} from 'rxjs';
import {NavigationExtras, Router} from '@angular/router';
import {ProductModelServer} from '../models/product.model';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private SERVER_URL = environment.SERVER_URL;

  //Store cart data on client's local storage
  private cartDataClient: CartModelPublic = {
    total: 0,
    proData: [{
      incart: 0,
      id: 0
    }]
  }

  //Data variable to store cart information on the server
  private cartDataServer: CartModelServer = {
    total: 0,
    data: [{
      numInCart: 0
    }]
  };

  //Observables for the components to subscribe
  cartTotal$ = new BehaviorSubject<number>(0);
  cartData$ = new BehaviorSubject<CartModelServer>(this.cartDataServer);

  constructor(private http: HttpClient,
              private productService: ProductService,
              private orderService: OrderService,
              private router: Router,
              private toast: ToastrService,
              private spinner: NgxSpinnerService) {
    this.cartTotal$.next(this.cartDataServer.total);
    this.cartData$.next(this.cartDataServer);

    //Get data from local storage (if any)
    let info: CartModelPublic = JSON.parse(<string> localStorage.getItem("cart"));
    if(info !== null && info !== undefined && info.proData[0].incart !== 0){
      //local storage has some items
      this.cartDataClient = info;
      //loop through entry and put it in cartDataServer
      this.cartDataClient.proData.forEach(p=>{
        this.productService.getSingleProduct(p.id).subscribe((actualProductInfo: ProductModelServer)=>{
          if(this.cartDataServer.data[0].numInCart === 0){
            this.cartDataServer.data[0].numInCart = p.incart;
            this.cartDataServer.data[0].product = actualProductInfo;
            this.CalculateTotal();
            this.cartDataClient.total = this.cartDataServer.total;
            localStorage.setItem('cart', JSON.stringify(this.cartDataClient));
          }else {
            //cartDataServer already has some entries
            this.cartDataServer.data.push({
              numInCart: p.incart,
              product: actualProductInfo
            });
            this.CalculateTotal();
            this.cartDataClient.total = this.cartDataServer.total;
            localStorage.setItem('cart', JSON.stringify(this.cartDataClient));
          }
          this.cartData$.next({...this.cartDataServer});
        })
      });
    }
  }
  AddProductToCart(id: number, quantity?: number){
    this.productService.getSingleProduct(id).subscribe(prod=>{
      //1. Cart is empty
      if(this.cartDataServer.data[0].product === undefined){
        this.cartDataServer.data[0].product = prod;
        this.cartDataServer.data[0].numInCart = quantity !== undefined ? quantity : 1;
        this.CalculateTotal();
        this.cartDataClient.proData[0].incart = this.cartDataServer.data[0].numInCart;
        this.cartDataClient.proData[0].id = prod.id;
        this.cartDataClient.total = this.cartDataServer.total;
        localStorage.setItem('cart', JSON.stringify(this.cartDataClient));
        this.cartData$.next({...this.cartDataServer})
        this.toast.success(`${prod.name} added to the cart`, 'Product Added', {
          timeOut: 1500,
          progressBar: true,
          progressAnimation: 'increasing',
          positionClass: 'toast-top-right'
        })
      }else {
        //2. Cart has items
        // @ts-ignore
        //  a. item already in the cart
        let index = this.cartDataServer.data.findIndex( p => p.product.id === prod.id);//-1 or positive value
        if(index !== -1){
          if(quantity !== undefined && quantity <= prod.quantity){
            this.cartDataServer.data[index].numInCart = this.cartDataServer.data[index].numInCart < prod.quantity?
              quantity : prod.quantity;
          }else {
            this.cartDataServer.data[index].numInCart < prod.quantity ?
              this.cartDataServer.data[index].numInCart++  : prod.quantity;
          }
          this.cartDataClient.proData[index].incart = this.cartDataServer.data[index].numInCart;
          this.CalculateTotal();
          this.cartDataClient.total = this.cartDataServer.total;
          localStorage.setItem('cart', JSON.stringify(this.cartDataClient));
          this.toast.info(`${prod.name} quantity updated in the cart`, 'Product Updated', {
            timeOut: 1500,
            progressBar: true,
            progressAnimation: 'increasing',
            positionClass: 'toast-top-right'
          })
        }else {
          //  b. item not in the cart
          this.cartDataServer.data.push({
            numInCart: 1,
            product: prod
          });

          this.cartDataClient.proData.push({
            incart: 1,
            id: prod.id
          });
          this.CalculateTotal();
          this.cartDataClient.total = this.cartDataServer.total;
          localStorage.setItem('cart', JSON.stringify(this.cartDataClient));
          this.toast.success(`${prod.name} added to the cart`, 'Product Added', {
            timeOut: 1500,
            progressBar: true,
            progressAnimation: 'increasing',
            positionClass: 'toast-top-right'
          })
          this.cartData$.next({...this.cartDataServer});
        }
      }
    })
  }
  UpdateCartItems(index: number, increase: boolean){
    let data = this.cartDataServer.data[index];
    if(increase){
      // @ts-ignore
      data.numInCart < data.product.quantity ? data.numInCart++ : data.product?.quantity;
      this.cartDataClient.proData[index].incart = data.numInCart;
      this.CalculateTotal();
      this.cartDataClient.total = this.cartDataServer.total;
      localStorage.setItem('cart', JSON.stringify(this.cartDataClient));
      this.cartData$.next({...this.cartDataServer});
    }else {
      data.numInCart--;
      if(data.numInCart < 1){
        // @ts-ignore
        this.DeleteProductFromCart(data.product.id)
        this.cartData$.next({...this.cartDataServer});
      }else {
        this.cartData$.next({...this.cartDataServer});
        this.cartDataClient.proData[index].incart = data.numInCart;
        this.CalculateTotal();
        this.cartDataClient.total = this.cartDataServer.total;
        localStorage.setItem('cart', JSON.stringify(this.cartDataClient));
      }
    }
  }
  DeleteProductFromCart(index: number) {
    if (window.confirm('Are you sure you want to delete the item?')) {
      this.cartDataServer.data.splice(index, 1);
      this.cartDataClient.proData.splice(index, 1);
      this.CalculateTotal();
      this.cartDataClient.total = this.cartDataServer.total;

      if (this.cartDataClient.total === 0) {
        this.cartDataClient = {total: 0, proData: [{incart: 0, id: 0}]};
        localStorage.setItem('cart', JSON.stringify(this.cartDataClient));
      } else {
        localStorage.setItem('cart', JSON.stringify(this.cartDataClient));
      }

      if (this.cartDataServer.total === 0) {
        this.cartDataServer = {
          data: [{
            product: undefined,
            numInCart: 0
          }],
          total: 0
        };
        this.cartData$.next({...this.cartDataServer});
      } else {
        this.cartData$.next({...this.cartDataServer});
      }
    }
    // If the user doesn't want to delete the product, hits the CANCEL button
    else {
      return;
    }


  }
  private  CalculateTotal(){
    let total = 0;
    this.cartDataServer.data.forEach(p=>{
      const {numInCart} = p;
      // @ts-ignore
      const {price} = p.product;

      total = total + numInCart * price;
    });
    this.cartDataServer.total = total;
    this.cartTotal$.next(this.cartDataServer.total);
  }
  CheckoutFromCart(userEmail: string){
    // @ts-ignore
    this.http.post(`${this.SERVER_URL}/orders/payment`, null).subscribe((res: {success: boolean})=>{
      if(res.success){
        let orderConfirmationRequest : OrderConfirmationRequest = {
          userEmail : userEmail,
          products: this.cartDataClient.proData
        }
        this.http.post(`${this.SERVER_URL}/orders/new`, orderConfirmationRequest
          // @ts-ignore
        ).subscribe((data: OrderConfirmationResponse) => {
          this.orderService.getSingleOrder(data.order_id).then(prods=>{
            if(data.success){
              console.log(data);
              this.resetServerData();
              const navigationExtras: NavigationExtras = {
                state : {
                  message: data.message,
                  products: prods,
                  orderId: data.order_id,
                  total: this.cartDataClient.total
                }
              };
              this.spinner.hide();
              this.router.navigate(['/thankyou'], navigationExtras).then(p=>{
                this.cartDataClient = {total: 0, proData:[{incart: 0, id: 0}]};
                this.cartTotal$.next(0);
                localStorage.setItem('cart', JSON.stringify(this.cartDataClient));
              });
            }else {
              this.spinner.hide();
              this.router.navigate(['/checkout']).then();
              this.toast.error(`Sorry, failed to book order!`, 'Booking Error', {
                timeOut: 1500,
                progressBar: true,
                progressAnimation: 'increasing',
                positionClass: 'toast-top-right'
              })
            }
          });
        });
      }else {
        this.spinner.hide();
        this.router.navigate(['/checkout']).then();
        this.toast.error(`Sorry, failed to book order! Payment error`, 'Booking Error', {
          timeOut: 1500,
          progressBar: true,
          progressAnimation: 'increasing',
          positionClass: 'toast-top-right'
        })
      }
    });
  }
  private resetServerData(){
    this.cartDataServer = {
      total: 0,
      data: [{
        numInCart: 0
      }]
    };
    this.cartData$.next({...this.cartDataServer});
  }
  CalculateSubTotal(index: number): number{
    let subTotal = 0;
    const  p = this.cartDataServer.data[index];
    // @ts-ignore
    subTotal = p.product.price * p.numInCart;
    return subTotal;
  }
}

interface OrderResponse {
  order_id: number;
  success: boolean;
  message: string;
  products: [{
    id: string,
    numInCart: string
  }];
}
interface OrderConfirmationResponse {
  order_id: number;
  success: boolean;
  message: string;
  products: [{
    id: string,
    numInCart: string
  }]
}
interface OrderConfirmationRequest {
  userEmail: string,
  products:   [
    {
      id: number,
      incart: number
    }
  ]
}

import { Injectable } from '@angular/core';
// @ts-ignore
import {AuthService, GoogleLoginProvider, SocialUser} from 'angularx-social-login';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {BehaviorSubject} from 'rxjs';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  // @ts-ignore
  auth: boolean;
  private SERVER_URL = environment.SERVER_URL;
  // @ts-ignore
  isUserSocial: boolean;
  // @ts-ignore
  authState$ = new BehaviorSubject(this.auth);
  // @ts-ignore
  userData$ = new BehaviorSubject<SocialUser|ResponseModel|{email: string}>({});
  // @ts-ignore
  socialUserToRegister$ = new BehaviorSubject<{socialUser: SocialUser, password: string}>();

  constructor(private authService: AuthService,
              private httpClient: HttpClient,
              private cookieService: CookieService, private router: Router,
              private http: HttpClient) {
    authService.authState.subscribe((user: SocialUser)=>{
      if(user!==null){
        this.auth = true;
        this.isUserSocial = true;
        this.authState$.next(this.auth);
        this.userData$.next(user);
        this.cookieService.set('email', user.email);
        this.socialUserToRegister$.next({socialUser: user, password: ""});
        localStorage.setItem('user', JSON.stringify(user));
      }
    });
  }

  loginUser(email: string, password: string){
    this.httpClient.post(`${this.SERVER_URL}/auth/login`, {email, password})
      // @ts-ignore
      .subscribe((data: ResponseModel)=>{
        this.auth = data.auth;
        this.isUserSocial = false;
        this.cookieService.set('email', data.email);
        localStorage.setItem('user', JSON.stringify(data));
        this.authState$.next(this.auth);
        this.userData$.next(data);
      });
  }

  googleLogin(){
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((user: SocialUser)=>{
      this.isUserSocial = true;
      this.cookieService.set('email', user.email);
      this.socialUserToRegister$.next({socialUser: user, password: ""});
      localStorage.setItem('user', JSON.stringify(user));
      this.http.get(`${this.SERVER_URL}/users/${user.email}`)
        // @ts-ignore
        .subscribe(data=>{
          // @ts-ignore
          if(data.success){
            this.router.navigate(['/profile']);
          }else {
            this.router.navigate(['/create-password']);
          }
        })
    });
  }

  logout(){
    this.authService.signOut().then();
    this.auth = false;
    this.isUserSocial = false;
    localStorage.setItem('user', '');
    this.cookieService.delete('email');
    this.authState$.next(this.auth);
    this.userData$.next({email: ''});
    this.router.navigate(['/']);
  }
}
interface ResponseModel {
  token: string;
  auth: boolean;
  email: string;
  username: string;
  fname: string;
  lname: string;
  photoUrl: string;
  userId: number;
}

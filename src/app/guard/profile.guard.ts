import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {UserService} from '../services/user.service';
import {CookieService} from 'ngx-cookie-service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfileGuard implements CanActivate {
  private SERVER_URL = environment.SERVER_URL;

  constructor(private userService: UserService,
              private router: Router, private cookieService: CookieService, private http: HttpClient) {
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(this.userService.auth || this.cookieService.get('email').length !==0){
        if(this.userService.isUserSocial){
          //Check if social user has account
          this.http.get(`${this.SERVER_URL}/users/${this.cookieService.get('email')}`)
            .subscribe(data=>{
              // @ts-ignore
              if(data.success){
                return true;
              }else {
                this.router.navigate(['/create-password'], {queryParams: {returnUrl: state.url, isMessage: true}});
                return false;
              }
            })
        }
        return true
      }
      this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
      return false
  }

}
